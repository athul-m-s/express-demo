const Joi = require("joi");
const express = require("express");
const app = express();

const courses = require("./routes/courses");
const home = require("./routes/home");

const logger = require("./middleware/logger");
const helmet = require("helmet");
//it logs everytime when there a http req happens
const morgan = require("morgan");
const config = require("config");

//set DEBUG=app:startup : so it only shows startup logs
//set DEBUG=app:startup,app:db
//set DEBUG=app:* //all debug module
//DEBUG=app:db nodemon.index.js , run and debug in one line
const startupDebugger = require("debug")("app:startup");
const dbDebugger = require("debug")("app:db");

// app.use(function(req,res,next) {
// console.log("something");
// //next return this to next middlewhare
// next();
// })

app.use(express.json());
//default middleware:parses req.body
//use x-www.form-urlencoded in postman
app.use(express.urlencoded({ extended: true }));
//get static assets through this, public is a folder name
//so localhost:3000/readme.txt will get that file
app.use(express.static("public"));
app.use(logger);

//to set various http headers : it checks headers
app.use(helmet());
app.use("/api/courses", courses);
app.use("/", home);
// const pug = require("pug"); NOTE:no need this ecpress automate it
app.set("view engine", "pug");
//optional setting to where it all stroes
app.set("views", "./views"); //views a folder that is def

//configuration
// getting config properties
//get values for dev and prod
console.log(config.get("name"));
console.log(config.get("mail.host"));

//the password set as env variable
console.log(config.get("mail.password"));

//enable morgan only in development
// also to get env process.env.NODE_ENV uses //def is undefined
//app.get("env") == "development" by default
if (app.get("env") == "development") {
  //it logs api details in the console
  app.use(morgan("tiny"));
  // console.log("morgan enbled"); instead of this startupdebugger can be use
  startupDebugger("morgan enbled");
}

// DB Works
dbDebugger("database connection succeed");

//to set env variable , in cmd
// set NODE_ENV=production  NOTE : developemnt is default
// set PORT=3000

const port = process.env.PORT || 3000;

app.listen(port, console.log("listening to port 3000"));
