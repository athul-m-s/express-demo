const express = require("express");
const app = express();

const courses = [
  { id: 1, name: "a" },
  { id: 2, name: "b" },
  { id: 3, name: "c" },
];

//route handler functions

// app.get("/api/courses/:year/:month",(req,res) => {
// get params by like this
//   req.params.month
//   req.params.year
//   req.query : which gives an object /api/post/2021/20?sortBy=name
// })

app.get("/", (req, res) => {
  res.send(courses);
});

app.get("/:id", (req, res) => {
  const course = courses.find((x) => {
    return x.id == parseInt(req.params.id);
  });

  if (!course) {
    res.status(404).send("The course with id not found");
    return;
  } else {
    res.send(course);
  }
});

app.post("/", (req, res) => {
  const { error } = validateCourse(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const course = {
    id: courses.length + 1,
    name: req.body.name,
  };

  courses.push(course);
  res.send(course);
});

app.put("/:id", (req, res) => {
  const { error } = validateCourse(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const course = courses.find((x) => {
    return x.id == parseInt(req.params.id);
  });

  if (!course) {
    res.status(404).send("The course with id not found");
    return;
  }

  course.name = req.body.name;
  res.send(course);
});

app.delete("/:id", (req, res) => {
  const course = courses.find((x) => {
    return x.id == parseInt(req.params.id);
  });

  if (!course) {
    res.status(404).send("The course with id not found");
    return;
  }

  const index = courses.indexOf(course);
  courses.splice(index, 1);

  res.send(course);
});

function validateCourse(course) {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
  });

  // response of validate : example
  // {
  //   error:{
  //     details:[{
  //       message:"name is required"
  //     }]
  //   },
  //   value:{name:'aaa'},
  //   then:[],
  //   catch:[]
  // }

  return schema.validate(course);
}

module.exports = app;
