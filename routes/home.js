const express = require("express");
const app = express();

app.get("/", (req, res) => {
  // res.send("Hello world !!");
  res.render("index", { title: "title of the app", message: "message" });
});

module.exports = app;
